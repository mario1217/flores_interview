# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160428151308) do

  create_table "bay_events", force: :cascade do |t|
    t.integer  "bay_id",              null: false
    t.integer  "visit_id"
    t.integer  "event_type"
    t.integer  "origin_type"
    t.text     "origin_data"
    t.datetime "origin_processed_at"
    t.datetime "transpired_at"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "bay_events", ["bay_id"], name: "index_bay_events_on_bay_id"
  add_index "bay_events", ["visit_id"], name: "index_bay_events_on_visit_id"

  create_table "bay_visits", force: :cascade do |t|
    t.integer  "bay_id",     null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "bay_visits", ["bay_id"], name: "index_bay_visits_on_bay_id"

  create_table "bays", force: :cascade do |t|
    t.integer  "remote_id",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end

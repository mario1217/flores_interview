class CreateBayEvents < ActiveRecord::Migration
  def change
    create_table :bay_events do |t|
      t.belongs_to :bay, index: true, null: false
      t.belongs_to :visit, index: true
      t.integer :event_type
      t.integer :origin_type
      t.text :origin_data
      t.datetime :origin_processed_at
      t.datetime :transpired_at

      t.timestamps null: false
    end
  end
end

class CreateBayVisits < ActiveRecord::Migration
  def change
    create_table :bay_visits do |t|
      t.belongs_to :bay, index: true, null: false

      t.timestamps null: false
    end
  end
end

class KafkaStreamer
  attr_accessor :topic, :consumer_label, :receiver_class
  DEFAULT_TIMEOUT = 180

  def initialize(topic:,consumer_label:,receiver_class:)
    self.topic = topic
    self.consumer_label = consumer_label
    self.receiver_class = receiver_class
  end

  def stream!
    puts "starting to receive #{topic} for #{DEFAULT_TIMEOUT/60} minutes"
    # Thread.new do
      Timeout.timeout(DEFAULT_TIMEOUT) do
        loop do
          responses = consumer.fetch
          responses.each do |response|
            puts response.value
            receiver_class.new(received_data: response.value).persist_data!
          end
        end
      # end
    end
  end

  private

  def consumer
    Poseidon::PartitionConsumer.new(
      consumer_label,
      "54.172.185.43",
      9092,
      topic,
      0,
      :earliest_offset
    )
  end
end

namespace :kafka do
  desc "receives bays from stream"
  task receive_bays: :environment do
    KafkaStreamer.new(
      topic: 'bays',
      consumer_label: 'bay_consumer',
      receiver_class: KafkaReceiver::Bays
    ).stream!
  end
  desc "receives bay_events from stream"
  task receive_bay_events: :environment do
    KafkaStreamer.new(
      topic: 'bay_events',
      consumer_label: 'bay_consumer',
      receiver_class: KafkaReceiver::BayEvents
    ).stream!
  end
  desc "receives all kafka events from stream"
  task receive_all: [:receive_bays, :receive_bay_events]
end
Rails.application.routes.draw do
  resources :bays, only: [:index, :show]
end

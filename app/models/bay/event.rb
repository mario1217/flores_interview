# == Schema Information
#
# Table name: bay_events
#
#  id                  :integer          not null, primary key
#  bay_id              :integer          not null
#  visit_id            :integer
#  event_type          :integer
#  origin_type         :integer
#  origin_data         :text
#  origin_processed_at :datetime
#  transpired_at       :datetime
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#

class Bay::Event < ActiveRecord::Base
  MINIMUM_OCCUPANCY = 2.minutes
  MINIMUM_MAINTENANCE = 2.minutes

  enum event_type: [:entry, :exit, :enter_maintenance, :exit_maintenance]
  enum origin_type: [:bays, :bay_events, :unknown]
  store :origin_data, accessors: [:id, :occupied, :out_of_service, :bay_id, :timestamp, :type], coder: JSON

  belongs_to :bay
  belongs_to :visit

  validates :bay, :event_type, :origin_type, presence: true

  after_create :associate_visit!

  def associate_visit!
    entry? ? start_visit! : end_visit!
  end

  private

  def start_visit!
    bay.visits.create!(entry_event: self)
  end

  def end_visit!
    last_visit = bay.visits.last
    if last_visit && last_visit.entry_event.exists?
      last_visit.update(exit_event: self)
    else
      create_visit_without_start!
    end
  end

  def create_visit_without_start!
    entry_event = Bay::Event.new(
      event_type: 'entry',
      bay: bay,
      origin_type: 'unknown',
      transpired_at: MINIMUM_OCCUPANCY.ago
    )
    bay.visits.create!(entry_event: entry_event, exit_event: self)
  end

end
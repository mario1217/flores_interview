# == Schema Information
#
# Table name: bay_visits
#
#  id         :integer          not null, primary key
#  bay_id     :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Bay::Visit < ActiveRecord::Base
  belongs_to :bay
  has_many :events
  has_one :entry_event, -> { entry }, class_name: "Bay::Event"
  has_one :exit_event, -> { where(event_type: 1) }, class_name: "Bay::Event"
  # using exit as a scope will actually exit you from the process

  def dwell
    entry_event - exit_event
  end

  def status?
    exit_event.exists? ? 'completed' : 'in progress'
  end

  private

  def start_time
    entry_event.transpired_at
  end

  def end_time
    exit_event.transpired_at
  end
end

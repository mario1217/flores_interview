# == Schema Information
#
# Table name: bays
#
#  id         :integer          not null, primary key
#  remote_id  :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Bay < ActiveRecord::Base
  has_many :visits
  has_many :events
  has_many :service_events, -> { bays }, class_name: "Bay::Event"
  has_many :occupation_events, -> { bay_events }, class_name: "Bay::Event"

  validates :remote_id, presence: true, uniqueness: true

  def total_visits
    visits.size
  end

  def occupied?
    occupation_events.any? && occupation_events.last.entry?
  end

  def in_service?
    service_events.any? && service_events.last.exit_maintenance?
  end
end



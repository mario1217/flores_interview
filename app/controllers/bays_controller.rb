class BaysController < ApplicationController
  def index
    @bays = Bay.limit(100).includes(:occupation_events, :service_events, :visits).all
    render json: @bays
  end

  def show
    @bay = Bay.includes(:visits).find(params[:id])
    render json: @bay
  end
end
# == KafkaReceiver::Bays
# Process data from Kfaka streams.  Has on attr - received_data. It is hash
# with the keys from our event:
# - bay_id: integer
# - timestamp: timestamp
# - type: string
class KafkaReceiver::BayEvents < KafkaReceiver

  def persist_data!
    bay = Bay.where(remote_id: received_data[:bay_id].to_s).first_or_create!
    bay.events.create!(
      event_type: received_data[:type],
      transpired_at: DateTime.parse(received_data[:timestamp]),
      origin_type: origin_type,
      origin_processed_at: DateTime.now,
      origin_data: received_data
    )
  end

end
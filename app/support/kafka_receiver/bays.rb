# == KafkaReceiver::Bays
# Process data from Kfaka streams.  Has on attr - received_data. It is hash
# with the keys from our event:
# - id: integer
# - occupied: boolean
# - out_of_service: boolean
class KafkaReceiver::Bays < KafkaReceiver

  def persist_data!
    bay = Bay.where(remote_id: received_data[:id]).first_or_create!
    puts bay.events.create!(
      event_type: event_type,
      transpired_at: DateTime.now, # No timestamp, so assume it just happened
      origin_type: origin_type,
      origin_processed_at: DateTime.now,
      origin_data: received_data
    )
  end

  private
    def event_type
      puts "#{received_data[:out_of_service]} is #{received_data[:out_of_service] ? true : false}"
      received_data[:out_of_service] ? 'enter_maintenance' : 'exit_maintenance'
    end
end




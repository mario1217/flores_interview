class KafkaReceiver
  attr_accessor :received_data

  # received_data should be a hash with the keys from the recieved topic
  def initialize(received_data:)
    received_data = eval(received_data) if received_data.is_a?(String) #not super safe.
    self.received_data = received_data
  end

  def persist_data!
    raise 'Unimplemented. Did you implement on your subclass?'
  end

  def origin_type
    self.class.name.demodulize.underscore
  end
end




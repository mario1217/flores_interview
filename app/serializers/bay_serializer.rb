# == Schema Information
#
# Table name: bays
#
#  id         :integer          not null, primary key
#  remote_id  :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class BaySerializer < ActiveModel::Serializer
  attributes :id, :remote_id, :total_visits, :visits, :occupied?, :in_service?
end

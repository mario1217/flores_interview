class Bays::EventSerializer < ActiveModel::Serializer
  attributes :id, :event_type, :origin_type
end

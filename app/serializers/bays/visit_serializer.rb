class Bay::VisitSerializer < ActiveModel::Serializer
  attributes :id, :start_time, :end_time, :dwell
end
